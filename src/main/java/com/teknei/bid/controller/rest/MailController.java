package com.teknei.bid.controller.rest;

import com.google.gson.Gson;
import com.teknei.bid.dto.MailRequestDTO;
import com.teknei.bid.dto.MailResponseDTO;
import com.teknei.bid.dto.OTPVerificationDTO;
import com.teknei.bid.dto.ResponseBase;
import com.teknei.bid.persistence.entities.BidClieMail;
import com.teknei.bid.persistence.entities.BidClieRegEsta;
import com.teknei.bid.persistence.entities.BidEstaProc;
import com.teknei.bid.persistence.entities.BidOtp;
import com.teknei.bid.persistence.entities.BidUsua;
import com.teknei.bid.persistence.repository.BidClieMailRepository;
import com.teknei.bid.persistence.repository.BidClieRegEstaRepository;
import com.teknei.bid.persistence.repository.BidEstaProcRepository;
import com.teknei.bid.persistence.repository.BidOtpRepository;
import com.teknei.bid.persistence.repository.BidUsuaRepository;
import com.teknei.bid.services.SendMailTask;
import com.teknei.bid.util.ErrorSmsCode;
import com.teknei.bid.util.SmsException;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.mail.MessagingException;
import java.security.SecureRandom;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(value = "/mail")
public class MailController {
	

    @Autowired
    private BidUsuaRepository bidUsuaRepository;

    @Autowired
    private BidOtpRepository otpRepository;
    @Autowired
    private SendMailTask mailTask;
    @Autowired
    private BidEstaProcRepository bidEstaProcRepository;
    @Autowired
    private BidClieRegEstaRepository bidClieRegEstaRepository;
    private static final String ESTA_PROC_OTP_REGISTERED = "ENV-REGOTP";
    private static final String ESTA_PROC_OTP_VALIDATED = "AUT-OTP";

    @Autowired
    private BidClieMailRepository bidClieMailRepository;
    
    private static final Logger log = LoggerFactory.getLogger(MailController.class);

    @ApiOperation(value = "Sends the generated contract for the related customer", response = String.class)
    @RequestMapping(value = "/contractSigned", method = RequestMethod.POST)
    public ResponseEntity<String> sendContract(@RequestBody MailRequestDTO mailRequestDTO) {
        try {
            mailTask.sendMailContract(mailRequestDTO.getIdClient());
            return new ResponseEntity<>("OK", HttpStatus.OK);
        } catch (MessagingException e) {
            log.error("Error sending mail with contract to customerId: {} with message: {}", mailRequestDTO, e.getMessage());
            return new ResponseEntity<>("ERROR", HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @ApiOperation(value = "Generates an OTP related to the customer", response = MailResponseDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "If the OTP could be saved successfully"),
            @ApiResponse(code = 422, message = "If the OTP could not be saved in relation to the customer")
    })
    @RequestMapping(value = "/OTP/resend", method = RequestMethod.POST)
    public ResponseEntity<MailResponseDTO> reGenerateOtp(@RequestBody MailRequestDTO mailRequestDTO) {
        try {
            List<BidOtp> bidOtpList = otpRepository.findByIdClie(mailRequestDTO.getIdClient());
            for (BidOtp o : bidOtpList) {
                o.setIdEsta(2);
                o.setFchModi(new Timestamp(System.currentTimeMillis()));
                o.setUsrOpeModi(mailRequestDTO.getUsername());
                otpRepository.save(o);
            }
        } catch (Exception e) {
            log.error("Error deactivating previous OTP: {}", e.getMessage());
        }
        ResponseEntity<MailResponseDTO> primaryResponse = generateOTP(mailRequestDTO);
        return primaryResponse;
    }

    @ApiOperation(value = "Generates an OTP related to the customer", response = MailResponseDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "If the OTP could be saved successfully"),
            @ApiResponse(code = 422, message = "If the OTP could not be saved in relation to the customer")
    })
    @RequestMapping(value = "/OTP", method = RequestMethod.POST)
    public ResponseEntity<MailResponseDTO> generateOTP(@RequestBody MailRequestDTO mailRequestDTO) {
        BidOtp otp = new BidOtp();
        otp.setFchCrea(new Timestamp(System.currentTimeMillis()));
        otp.setIdClie(mailRequestDTO.getIdClient());
        otp.setOtp(generatePin());
        otp.setUsed(false);
        otp.setSend(false);
        otp.setUsrCrea(mailRequestDTO.getUsername());
        otp.setUsrOpeCrea(mailRequestDTO.getUsername());
        otp.setIdEsta(1);
        otp.setIdTipo(3);
        try {
            otpRepository.save(otp);
            updateStatus(mailRequestDTO.getIdClient(), ESTA_PROC_OTP_REGISTERED, mailRequestDTO.getUsername());
        } catch (Exception e) {
            log.error("Error saving otp for: {} with message: {}", mailRequestDTO, e.getMessage());
            return new ResponseEntity<>((MailResponseDTO) null, HttpStatus.UNPROCESSABLE_ENTITY);
        }
        MailResponseDTO responseDTO = new MailResponseDTO();
        responseDTO.setOtp(otp.getOtp());
        return new ResponseEntity<>(responseDTO, HttpStatus.OK);
    }

    @ApiOperation(value = "Generates an OTP related to the customer. The format is alpha-numeric and it must be sent in mail", response = MailResponseDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "If the OTP could be saved successfully"),
            @ApiResponse(code = 422, message = "If the OTP could not be saved in relation to the customer")
    })
    @RequestMapping(value = "/OTP/vc", method = RequestMethod.POST)
    public ResponseEntity<MailResponseDTO> generateOTPaz(@RequestBody MailRequestDTO mailRequestDTO) {
        BidOtp otp = new BidOtp();
        otp.setFchCrea(new Timestamp(System.currentTimeMillis()));
        otp.setIdClie(mailRequestDTO.getIdClient());
        otp.setOtp(generateAlphanumeric());
        otp.setUsed(false);
        otp.setSend(false);
        otp.setUsrCrea(mailRequestDTO.getUsername());
        otp.setUsrOpeCrea(mailRequestDTO.getUsername());
        otp.setIdEsta(1);
        otp.setIdTipo(3);
        try {
            otpRepository.save(otp);
            updateStatus(mailRequestDTO.getIdClient(), ESTA_PROC_OTP_REGISTERED, mailRequestDTO.getUsername());
        } catch (Exception e) {
            log.error("Error saving otp for: {} with message: {}", mailRequestDTO, e.getMessage());
            return new ResponseEntity<>((MailResponseDTO) null, HttpStatus.UNPROCESSABLE_ENTITY);
        }
        MailResponseDTO responseDTO = new MailResponseDTO();
        responseDTO.setOtp(otp.getOtp());
        return new ResponseEntity<>(responseDTO, HttpStatus.OK);
    }



    @ApiOperation(value = "Validates OTP and uses it", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Validation successfull"),
            @ApiResponse(code = 404, message = "No OTP found"),
            @ApiResponse(code = 422, message = "Error validating OTP")
    })
    @RequestMapping(value = "/validateOTP", method = RequestMethod.POST)
    public ResponseEntity<String> validateOTP(@RequestBody OTPVerificationDTO dto) {
        try {
            List<BidOtp> list = otpRepository.findByIdClieAndOtpAndUsed(dto.getIdClient(), dto.getOtp(), false);
            if (list == null || list.isEmpty()) {
                return new ResponseEntity<>((String) null, HttpStatus.NOT_FOUND);
            }
            BidOtp target = list.get(0);
            target.setUsed(true);
            target.setFchModi(new Timestamp(System.currentTimeMillis()));
            target.setUsrModi(dto.getUsername());
            target.setUsrOpeModi(dto.getUsername());
            target.setIdEsta(2);
            otpRepository.save(target);
            updateStatus(dto.getIdClient(), ESTA_PROC_OTP_VALIDATED, dto.getUsername());
            return new ResponseEntity<>("OK", HttpStatus.OK);
        } catch (Exception e) {
            log.error("Error validating OTP for request:{} with message: {}", dto, e.getMessage());
            return new ResponseEntity<>((String) null, HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    private String generatePin() {
        SecureRandom random = new SecureRandom();
        int num = random.nextInt(1000000);
        String formatted = String.format("%06d", num);
        return formatted;
    }

    private String generateAlphanumeric(){
        String random = UUID.randomUUID().toString();
        return random;
    }

    private void updateStatus(Long idClient, String status, String username) {
        try {
        	
            BidEstaProc estaProc = bidEstaProcRepository.findTopByCodEstaProcAndIdEsta(status, 1);
            BidClieRegEsta regEsta = bidClieRegEstaRepository.findByIdClieAndIdEstaProc(idClient, estaProc.getIdEstaProc());
            if (regEsta == null) {
                log.warn("Status for process: {} found null", idClient);
            }
            regEsta.setEstaConf(true);
            regEsta.setFchModi(new Timestamp(System.currentTimeMillis()));
            regEsta.setUsrModi(username);
            regEsta.setUsrOpeModi(username);
            bidClieRegEstaRepository.save(regEsta);
        } catch (Exception e) {
            log.error("Error finding status of process for customer: {} with message: {}", idClient, e.getMessage());
        }
    }
    
	// TODO AJGD --------------------------------------------------------------------------->>>
    
	@ApiOperation(value = "Sends sms wen forget password", response = String.class)
	@RequestMapping(value = "/forgetPassword/{user}/{mail}", method = RequestMethod.GET)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Validation successfull"),
            @ApiResponse(code = 500, message = "Error sending sms")
    })
	public ResponseEntity<String> forgetPassword(@PathVariable String user, @PathVariable String mail) {
		log.info("MailController - forgetPassword");    	
		Timestamp now = new Timestamp(System.currentTimeMillis());	
		Gson gson = new Gson();
		try {
			// Obteniendo idClie...
			List<BidUsua> bidUserList = bidUsuaRepository.findByUsua(user);
			BidUsua bu = null;
			if (bidUserList != null && !bidUserList.isEmpty()) {
				bu = bidUserList.get(0);
			} else {
				log.info("MailController - forgetPassword :No se encontro al usuario.");
				return new ResponseEntity<>(gson.toJson(new ResponseBase(HttpStatus.NOT_FOUND.toString(),
						"No se encontro al usuario " + user, "FAIL")).toString(), HttpStatus.OK);
			}			
			
			BidClieMail clieMail = bidClieMailRepository.findTopByIdClie(bu.getIdClie());
			String tlds = "com";
			try {
				String dbEmail = clieMail.getEmai();
				log.info("> dbEmail: " + dbEmail );
				String[] tldsArray = dbEmail.split("\\.");
				tlds = tldsArray[tldsArray.length - 1];				
			} catch (Exception e) {
				log.info("Error: ",e);
			}			
			String mailCompleto = mail+"."+tlds;
			log.info("> inputEmail: " + mailCompleto );
			if(!clieMail.getEmai().toUpperCase().equals(mailCompleto.toUpperCase())) {
				log.info("MailController - forgetPassword :El correo no cincide.");
				return new ResponseEntity<>(gson.toJson(new ResponseBase(HttpStatus.NOT_FOUND.toString(),
						"El correo ingresado no coincide con el usuario " + user, "FAIL")).toString(), HttpStatus.OK);
			}
			// TODO validar 3 intentos mandar mensaje diferente
			List<BidOtp> bidOtpList1 = otpRepository.findByIdClie(bu.getIdClie());
			List<BidOtp> activos = new  ArrayList<BidOtp>(); 
		    List<BidOtp> bidOtpList = new  ArrayList<BidOtp>();   
			for(BidOtp bOtp : bidOtpList1) {	
				 if(bOtp.isActivo()) {
					 activos.add(bOtp);
				 }
				 if(!bOtp.isUsed()){ 
					 bidOtpList.add(bOtp);
				 }
			}
			log.info("> activos"+activos.size());
			//validando vigencia y no usados
//			  List<BidOtp> bidOtpList = otpRepository.findByIdClieAndUsed(bu.getIdClie(), false);				  
			  if(bidOtpList != null && !bidOtpList.isEmpty()) {  
				  List<BidOtp> vigentes = new  ArrayList<BidOtp>();  
				  for(BidOtp bOtp : bidOtpList) {					 
					 if(now.after(bOtp.getFchCadu())){ 
						 //Eliminando caducados..
						 bOtp.setUsed(true);
						 bOtp.setFchModi(new Timestamp(System.currentTimeMillis()));
						 bOtp.setUsrModi("system");
						 bOtp.setUsrOpeModi("system");
						 bOtp.setIdEsta(10);
						 bOtp.setActivo(false);		//ESTE va con F2
				         otpRepository.save(bOtp);
					 }else {
						 vigentes.add(bOtp);						
					 }					 
				  }

			log.info("> vigentes"+vigentes.size());
				 if (vigentes.size() > 2) {
						log.info("MailController - forgetPassword:  El numero codigos de validacion enviados ha sido exedido.");
						return new ResponseEntity<>(gson.toJson(new ResponseBase(HttpStatus.LOOP_DETECTED.toString(),
								"El número de codigos de validación enviados ha sido excedido, intentalo mas tarde.", "FAIL")).toString(), HttpStatus.OK);
				 }
//				 if(activos.size() > 2) {  proceso F2
//					 //si ya pasaron mas de 15 minutos del ultimo mensaje reeiniciar el contador
//					 log.info("MailController - forgetPassword:  El numero codigos de validacion enviados ha sido exedido. es necesario reiniciar proceso de solicitud de password.");
//					 for(BidOtp bOtp : bidOtpList1) {
//						 //Eliminando caducados..
//						 bOtp.setUsed(true);
//						 bOtp.setFchModi(new Timestamp(System.currentTimeMillis()));
//						 bOtp.setUsrModi("system");
//						 bOtp.setUsrOpeModi("system");
//						 bOtp.setIdEsta(10);	
//						 bOtp.setActivo(false);		
//				         otpRepository.save(bOtp); 
//					 }			 
//					 return new ResponseEntity<>(gson.toJson(new ResponseBase(HttpStatus.LOCKED.toString(),
//								"El número de códigos de validación enviados ha sido excedido, es necesario reiniciar proceso de recuperación de contraseña.", "FAIL")).toString(), HttpStatus.OK);
//				 }
				 
			  }
			// Creando Otp...
			BidOtp otp = new BidOtp();
			otp.setFchCrea(new Timestamp(System.currentTimeMillis()));
			otp.setIdClie(bu.getIdClie());
			otp.setOtp(generatePin());
			otp.setUsed(false);
			otp.setUsrCrea("NA");
			otp.setUsrOpeCrea("NA");
			otp.setIdEsta(9);
			otp.setIdTipo(9);
			otp.setActivo(true);
			otp.setFchCadu(expireDate());
			// Enviando SMS...
			mailTask.sendSMS2(otp);
			return new ResponseEntity<>(gson.toJson(new ResponseBase(HttpStatus.OK.toString(),
							"SMS enviado.","SUCCESS")).toString(), HttpStatus.OK);
		} catch (SmsException e) {
			log.error("SMS Error reached: {} at: {}", e.getMessage(), System.currentTimeMillis());
            if (e.getMessage().contains(ErrorSmsCode.INTERNAL_API_NUMBER_NOT_VALID.name()) || e.getMessage().contains(ErrorSmsCode.INTERNAL_API_NOT_NUMBER_PROVIDED.name())) {
                log.info("Send backup OTP via email");
                //  mailTask.sendMail;
            }  
    		return new ResponseEntity<>(gson.toJson(new ResponseBase(HttpStatus.INTERNAL_SERVER_ERROR.toString(),
					"Error al enviar el sms: "+e.getMessage(),"FAIL")).toString(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
	
//	private boolean valida15min(List<BidOtp> activos) {
//		 for(BidOtp bOtp : activos) {
//		 
//		 }
//		return false;
//	}
	
	/**
	 * Genera la fecha de caducidad del otp a 10  minutos
	 * @return
	 */
    private Timestamp expireDate() {
    	Calendar cal = Calendar.getInstance(); 
    	cal.add(Calendar.MINUTE,5);
    	return new Timestamp(cal.getTimeInMillis());
    }
    private Timestamp sumaMinDate(int min) {
    	Calendar cal = Calendar.getInstance(); 
    	cal.add(Calendar.MINUTE,min);
    	return new Timestamp(cal.getTimeInMillis());
    }
    
    
    @ApiOperation(value = "Validates OTP and uses it to forget Password", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Validation successfull"),
            @ApiResponse(code = 404, message = "No OTP found"),
            @ApiResponse(code = 408, message = "OTP expirated"),
            @ApiResponse(code = 500, message = "Error validating OTP")
    })
    @RequestMapping(value = "/forgetPassword/validateOTP", method = RequestMethod.POST)
    public ResponseEntity<String> forgetPasswordValidateOTP(@RequestBody OTPVerificationDTO dto) {
    	log.info("MailController - forgetPasswordValidateOTP");
        try {
			// Obteniendo idclie pod username
			List<BidUsua> bidUserList = bidUsuaRepository.findByUsua(dto.getUsername());
			if (bidUserList == null || bidUserList.isEmpty()) {
				log.error("MailController - forgetPasswordValidateOTP: no se encontro usuario");
				return new ResponseEntity<>("No se encontro usuario", HttpStatus.NOT_FOUND);
			}
			dto.setIdClient(bidUserList.get(0).getIdClie());	
			// Obteniendo otp
            List<BidOtp> list = otpRepository.findByIdClieAndOtpAndUsed(dto.getIdClient(), dto.getOtp(), false);            
            if (list == null || list.isEmpty()) {
				log.error("MailController - forgetPasswordValidateOTP: no se encontro OTP");
                return new ResponseEntity<>("No se encontro OTP", HttpStatus.NOT_FOUND);
            }
            //establece el codigo como usado.
            BidOtp target = list.get(0);
            target.setUsed(true);
            target.setFchModi(new Timestamp(System.currentTimeMillis()));
            target.setUsrModi(dto.getUsername());
            target.setUsrOpeModi(dto.getUsername());
            target.setIdEsta(10);			
            otpRepository.save(target);
            //checa vigencia.
            if (target.getFchCadu().before(new Timestamp(System.currentTimeMillis()))) {
            	log.error("Expiro el codigo de verificacion OTP: "+dto.getOtp()+" Usr:"+dto.getUsername()+" ExplireDate:"+target.getFchCadu());
				return new ResponseEntity<>("Codigo expirado", HttpStatus.REQUEST_TIMEOUT);
			}else {
				log.error("MailController - forgetPasswordValidateOTP: OK");
				return new ResponseEntity<>("OK", HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("Error validating OTP for request:{} with message: {}", dto, e.getMessage());
            return new ResponseEntity<>( e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //<<--------

}