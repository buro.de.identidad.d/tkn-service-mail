package com.teknei.bid.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class ResponseBase implements Serializable{
	private static final long serialVersionUID = -2806559398657325468L;
	
	private String code;
    private String message;
    private String status;
    
	public ResponseBase(String code, String message, String status) {
		super();
		this.code = code;
		this.message = message;
		this.status = status;
	}
    
    

}