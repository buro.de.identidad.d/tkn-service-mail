package com.teknei.bid.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class MailResponseDTO implements Serializable {

    private String otp;

}